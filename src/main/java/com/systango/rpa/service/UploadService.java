package com.systango.rpa.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.systango.rpa.dto.ErrorDto;
import com.systango.rpa.request.FileUploadRequest;

@Service
public class UploadService {

	public ResponseEntity<Object> uploadFile(FileUploadRequest fileUploadRequest) throws Exception {
		
		final String newFilePath = "C:"+File.separator+"Users"+File.separator+"Administrator"+File.separator+"Documents"+File.separator+"cas"+File.separator+"new"+File.separator;
		final String processedFilePath = "C:"+File.separator+"Users"+File.separator+"Administrator"+File.separator+"Documents"+File.separator+"cas"+File.separator+"processed"+File.separator;
		
		MultipartFile file = fileUploadRequest.getFile();
		if (file == null || file.getSize() == 0) {
			return ResponseEntity.ok(new ErrorDto("Please upload file"));
		} 
		
		Path fileStorageLocationPath = Paths.get(newFilePath + file.getOriginalFilename());

		Files.copy(file.getInputStream(), fileStorageLocationPath, StandardCopyOption.REPLACE_EXISTING);
		
		if (fileUploadRequest.getPassword() != null) {
			PrintWriter writer;

			writer = new PrintWriter(newFilePath + file.getOriginalFilename().split("\\.")[0] + ".txt", "UTF-8");
			writer.println(fileUploadRequest.getPassword());
			writer.close();

		}
		

		int count = 1;
		while (count <= 6) {
			String jsonFileName = processedFilePath + file.getOriginalFilename().split("\\.")[0] + "_json.json";
			File jsonFile = new File(jsonFileName);
			boolean fileExists = jsonFile.exists();
			if (fileExists) {

				BufferedReader br = new BufferedReader(new FileReader(jsonFile));

				StringBuffer stb = new StringBuffer();
				String st;
				while ((st = br.readLine()) != null) {
					stb.append(st);
				}
				ObjectMapper om = new ObjectMapper();
				Object responseObj = om.readValue(stb.toString(), Object.class);
				br.close();
				return ResponseEntity.ok(responseObj);
			}
			count++;
			Thread.currentThread().sleep(5000);
		}
		return ResponseEntity.ok(new ErrorDto("Something went wrong."));
	}
}
