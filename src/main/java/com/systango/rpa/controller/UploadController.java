package com.systango.rpa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.systango.rpa.request.FileUploadRequest;
import com.systango.rpa.service.UploadService;

@RestController
@RequestMapping("/file")
public class UploadController {
	
	@Autowired
	private UploadService uploadService;

	@PostMapping("/upload")
	public ResponseEntity<Object> uploadFile(@ModelAttribute FileUploadRequest fileUploadRequest) throws Exception {
		
		return uploadService.uploadFile(fileUploadRequest);
	}
}
