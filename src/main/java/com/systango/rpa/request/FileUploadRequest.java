package com.systango.rpa.request;

import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FileUploadRequest {

	@JsonIgnore
	private MultipartFile file;
	private String password;
}
